# Reporting Script

## Settings
The settings.conf should be populated as follows:
```
# settings.conf
# Tasks

# Events
export CALDAV_URL='https://cloud.example.com/caldav.php/'
export CALDAV_USR='username'
export CALDAV_PASSWD='calda-vpa55-w0rd'

# Weather
export METOFFICE_KEY='metoff-icet-0k3n-f0ra-p1'  ## API key
export METOFFICE_REGION='001'                    ## Region ID

# Mail
export MAIL_ADDRESS='reporting@example.com'      ## From
export MAIL_SERVER='mail.example.com'            ## Server
export MAIL_RECIPIENT='me@example.com'           ## To
```

The weather section needs a login to the MetOffice DataPoint (https://www.metoffice.gov.uk/datapoint/). The region ID can be found by interrogating (http://datapoint.metoffice.gov.uk/public/data/txt/wxfcs/regionalforecast/json/sitelist?key=<YOUR API KEY>).

## Kickoff Script
```
# kickoff.sh
/usr/bin/task sync 2>/dev/null >/dev/null
cd /path/to/working/dir/reporting/
source settings.conf
/usr/bin/python3 reporting.py
```
