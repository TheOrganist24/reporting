from datetime import datetime, timedelta
from taskw import TaskWarrior

task = TaskWarrior()


def retrieve_daily():
    data = {}
    yesterday = datetime.today() - timedelta(days=1)
    tasks = task.load_tasks()

    n = 0
    for completed_task in tasks['completed']:
        task_date = datetime.strptime(completed_task['end'], '%Y%m%dT%H%M%SZ')
        
        if task_date.date() == yesterday.date():
            n+=1
    data['yesterday'] = n

    l = []
    for pending_task in tasks['pending']:
        if 'scheduled' in pending_task:
            task_date = datetime.strptime(pending_task['scheduled'], '%Y%m%dT%H%M%SZ') + timedelta(days=1)

            if task_date.date() == datetime.now().date():
                l.append(pending_task)
    data['today'] = l

    return data


def retrieve_weekly():
    print('weekly')
    return


def retrieve_monthly():
    print('monthly')
    return

