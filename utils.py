import datetime
import os
import smtplib
from email.mime.text import MIMEText
from jinja2 import Environment
from jinja2 import FileSystemLoader

def compile(date, report, data):
    j2_env = Environment(loader=FileSystemLoader('templates'), trim_blocks=True)
    template = j2_env.get_template('document_daily.j2')
    document = template.render(date=date, report=report, data=data)
    
    return document


def mail(document):
    mail_address = os.environ.get('MAIL_ADDRESS')
    mail_server = os.environ.get('MAIL_SERVER')
    mail_recipient = os.environ.get('MAIL_RECIPIENT')

    msg = MIMEText(document, 'html')
    msg['Subject'] = 'Daily Summary - ' + datetime.datetime.today().strftime('%Y %m %d')
    msg['To'] = mail_recipient
    msg['From'] = mail_address
    msg.IsBodyHtml = True
    
    s = smtplib.SMTP(mail_server)
    s.sendmail(mail_address, mail_recipient, msg.as_string())
    s.quit
