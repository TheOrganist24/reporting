import caldav
import os
import requests
from datetime import datetime, date, timedelta
from caldav.elements import dav, cdav
from ics import Calendar

# Get event URLs

def retrieve_daily():
    url = os.environ.get('CALDAV_URL')
    user = os.environ.get('CALDAV_USR')
    passwd = os.environ.get('CALDAV_PASSWD')
    
    date_start = datetime.today().replace(hour=0, minute=0, second=0, microsecond=0, tzinfo=None)# - timedelta(days=0)
    date_end = datetime.today().replace(hour=0, minute=0, second=0, microsecond=0, tzinfo=None) + timedelta(days=1)
    
    client = caldav.DAVClient(url, username=user, password=passwd)
    principal = client.principal()
    calendars = principal.calendars()
    if len(calendars) > 0:
    
        data=[]
        for calendar in calendars:
            cal_data={}
            cal_data['calendar'] = calendar.name
            results = calendar.date_search(date_start, date_end)
    
            items=[]
            for event in results:
                c = Calendar(requests.get(event.url, auth=(user, passwd)).text)
    
                for item in c.events:
                    if item.name is not None:
                        items.append(item.name)
            if items is not None:
                cal_data['events'] = list(set(items))
                data.append(cal_data)
    return data
