import datetime
import events
import tasks
import utils
import weather

date = datetime.datetime.today().strftime('%A, %d %B %Y')
report = 'daily'

# getopts

# Get data
## daily
data = {}

data['events'] = events.retrieve_daily()
data['task'] = tasks.retrieve_daily()
data['weather'] = weather.retrieve_daily()

## weekly
## monthly


# compile into pdf or html
document = utils.compile(date, report, data)

# mail
utils.mail(document)
