import json
import requests
import os


def retrieve_daily():
    key = os.environ.get('METOFFICE_KEY')
    region = os.environ.get('METOFFICE_REGION')
    metoffice_url = 'http://datapoint.metoffice.gov.uk/public/data/txt/wxfcs/regionalforecast/json/' + region + '?key=' + key

    raw_data = requests.get(metoffice_url)
    forecast_data = json.loads(raw_data.text)
    data = forecast_data['RegionalFcst']['FcstPeriods']['Period'][0]['Paragraph']

    return data


def retrieve_weekly():
    print('weekly')
    return


def retrieve_monthly():
    print('monthly')
    return
